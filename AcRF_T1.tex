% Created 2016-08-11 Thu 11:11
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\documentclass[11pt]{article}
\usepackage[authoryear]{natbib}
\bibliographystyle{ieeetr}
\author{Ng Wee Kiat Jeremy}
\date{August-2016}
\title{A unified model of TGF-$\beta$ induced cytostasis in HaCaT}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={Emacs 24.3.1 (Org mode 8.2.10)}}
\begin{document}

\maketitle
\newpage
\section{ABSTRACT OF PROPOSAL}
\label{sec-1}
The TGF-$\beta$ pathway plays numerous roles in a range of different contexts: promoting cellular differentiation in development, migration in wound healing, and cell-cycle arrest in a range of epithelial cell types. Pardoxically, TGF-$\beta$ serves as a tumor suppressor in early carcinogenesis and as a promoter of tumor migration and metastasis in late-stage carcinogenesis. Although the TGF-$\beta$ signaling pathway has been well-studied and its basic biology well-understood, much remains unknown about the context-dependent regulation of its signaling output. 

Many studies have focused on the genomic regulation of TGF-$\beta$ signaling. Recent studies also highlight an important role of signaling dynamics such as stimulus strength and duration in determining the signaling output of TGF-$\beta$ signaling. However, we lack tools for precise control over the dynamic input of TGF-$\beta$ signaling. The emerging field of optogenetics -- engineered systems that are controlled by light -- allows us an opportunity to fill this gap by allowing precise temporal and spatial control over cell signaling.   

The human kerotinocyte cell line, HaCaT, is commonly used as a cell model to study TGF-$\beta$ induced cell-cycle arrest. Furthermore, extensive work has gone into developing mathematical models of TGF-$\beta$ signaling dynamics in HaCaT. Although the molecular pathway by which TGF-$\beta$ induces cell-cycle arrest in HaCaT is well-characterized, how the dynamics of TGF-$\beta$ signaling relates to the dynamics of cell-cycle arrest is not. It is this gap that we will address in this project.

In \textbf{Aim 1}, we will establish a stable optogenetic system that can be used to precisely manipulate the initiation of TGF-$\beta$ signaling in HaCaT. We hypothesize that \uline{the optogenetic system is able to recapitulate ligand-based activation of TGF-$\beta$ signaling}. Following the validation of the optogenetic system, we will then engineer stable cell lines with light-sensitive TGF-$\beta$ receptors. Because HaCaT cells have been observed to secrete TGF-$\beta$, we will simultaneously knock-out the native TGF-$\beta$ receptors of the HaCaT cells with the optogenetic constructs. 

Earlier studies have demonstrated that following TGF-$\beta$ stimulation, cells display a `refractory` state wherein they are no longer responsive to TGF-$\beta$ ligand. However, the time-scale of this refractory state differs: in one study, cells were re-sensitized 2 hours following ligand removal, whereas in another study, this refractory period persisted for about 6 hours. We hypothesize that \uline{the refractory period is determined by the deactivation dynamics of TGF-$\beta$ signaling}. Capitalizing on the rapid onset of activation and deactivation in our optogenetics system, we will refine existing models of TGF-$\beta$ signaling deactivation in \textbf{Aim 2}. 

p21 is a key player in TGF-$\beta$ induced cytostasis. Although studies have demonstrated an increase in mRNA levels 6-hours post-activation, cells were observed to require about 16 hours of sustained exposure to TGF-$\beta$ before cell cycle is arrested. This corresponds to experimental observations that p21 protein levels only increased about 16-hour post-activation. In \textbf{Aim 3}, we will quantify p21 transcription and translation dynamics, and thereafter, develop a quantitative model to explain the observed lag between mRNA transcription and protein translation. We hypothesize that \uline{p21 levels are regulated pre-translationally by potentially novel feedback mechanism}. We will use our model to identify possible feedback loops or regulatory mechanisms that explains the lag in protein translation, and validate our model of translational regulation of p21. We will further integrate our model of p21 regulation with existing models of TGF-$\beta$ signaling in order to characterize how the dynamics of TGF-$\beta$ signaling dictates the dynamics of p21 transcription and translation. Using our model, we will identify key steps along the TGF$\beta$-p21 signaling axis that serves to temporally regulate TGF-$\beta$ induced cell cycle arrest. Our predictions will then be experimentally tested. 

\newpage
\section{DETAILS OF RESEARCH PROPOSAL}
\label{sec-2}
\subsection{SPECIFIC AIMS}
\label{sec-2-1}
The transforming growth factor beta (TGF-$\beta$) family of cytokines are involved in the regulation of a diverse range of cellular processes, including cellular proliferation, apoptosis\cite{siegel2003cytostatic} and the induction of the epithelial-mesenchymal transition program\cite{zavadil2005tgf}. TGF-$\beta$ plays a paradoxical role in carcinogenesis, where it functions as a tumor suppressor in early carcinogenesis\cite{siegel2003cytostatic} but promotes invasion in late carcinogenesis. Clinically, over-expression of TGF-$\beta$ has been found to be associated with poor patient prognosis.

\begin{figure}[htb]
\centering
\includegraphics[width=.9\linewidth]{./Figure1.pdf}
\caption{\label{fig:pathway}\underline{Overview of the canonical TGF-$\beta$ signaling pathway.} (1) Signaling is initiated by the binding of a TGF-$\beta$ ligand to the receptor. (2) Receptor dimerization and cross-phosphorylation occurs, which then leads to the phosphorylation of an rSmad (3). The rSmad then complexes with Smad4 (4), and the rSmad-Smad4 complex translocates into the nucleus (5) where it serves as a transcription factor to promote expression of target genes (6).}
\end{figure}

The TGF-$\beta$ signaling pathway has a simple molecular architecture (Figure \ref{fig:pathway}) despite the range of phenotypes it is able to mediate. Signaling is initiated by the binding of a TGF-$\beta$ ligand to a TGF-$\beta$ receptor II receptor (TGF-$\beta$ RII). The Type II receptor then phosphorylates a Type I receptor (TGF-$\beta$ RI), leading to the dimerization of the receptor pair. The dimerization of the receptor acts as a kinase that then phosphorylates an rSmad (commonly Smad2 or Smad3). The phosphorylated rSmad then binds to Smad4, forming a heterodimeric complex that translocates into the nucleus to serve as a transcription factor. TGF-$\beta$ signaling is then terminated either via the depletion of ligand or via a range of negative feedback loops. 

Despite being the subject of intense study, how cells interpret TGF-$\beta$ signaling input to elicit the appropriate cellular output remains an elusive question. Although TGF-$\beta$ signaling output has been shown to be context-dependent, an emerging body of literature suggests an important role of stimuli strength\cite{clarke2009transforming} and duration\cite{zhang2014tgf,gal2008sustained}. Yet, existing ligand-based approaches are ill-suited for studying these dynamic factors due to the lack of precise control over both ligand concentration and termination of pathway activation. Furthermore, TGF-$\beta$ sensitive cells often demonstrate autocrine feedback wherein either TGF-$\beta$ or its regulators are secreted to modulate TGF-$\beta$ signaling activity. Hence, the development of a system that allows for precise spatiotemporal control of pathway initiation, as well as decoupling of pathway initiation from autocrine signaling, will enable us to precisely dissect the role of dynamics in TGF-$\beta$ signaling. 

We have developed an optogenetic system that possess the above-mentioned capability. The optogenetic system enables us to rapidly initiate and terminate TGF-$\beta$ signaling in a ligand-independent manner. Our system uses light to induce dimerisation of the cytoplasmic domains of TGF-$\beta$ RI and TGF-$\beta$ RII, and is able to recapitulate ligand-induced TGF-$\beta$ signaling (see \textbf{PRELIMINARY STUDIES}). We will use the work of this grant to rigorously dissect the role of TGF-$\beta$ signaling dynamics in the induction of cell cycle arrest in HaCaT. 

\begin{figure}[htb]
\centering
\includegraphics[width=.9\linewidth]{./Figure2.png}
\caption{\label{fig:optogenetics}Optogenetic system designed for manipulation of TGF-$\beta$ signaling. \emph{A.}\underline{Optogenetic components.} The plant protein PhyB binds to PCB to form a photo-sensitive holoenzyme. Upon exposure to 650nm light, the holoenzyme undergoes a conformational change that allows it to bind to PIF. The binding of PhyB to PIF can be rapidly reversed upon exposure to 750nm light. \emph{B.}\underline{Molecular constructs.} The cytoplasmic domain of TGF-$\beta$ receptor I (residues 155-503) is fused mCherry, PhyB and the KrasBCT domain, which anchors the construct to the membrane. The entire construct is placed under the control of a CMV promoter, and cloned into the pcDNA3.1A expression vector. On the other hand, the cytoplasmic domain of TGF-$\beta$ receptor II (residues 193-567) is fused to YFP and PIF6APB, and placed under the control of an SV40 promoter. Similarly, the construct is cloned into the pcDNA3.1 vector for expression.}
\end{figure}
\newline
\uline{\textbf{Aim 1}} will establish and validate the proposed optogenetic system for reversible activation of TGF-$\beta$ signaling (Figure 2). Initial validation will be performed using transiently transfected cells; thereafter, we will stably transfect the HaCaT cells with the validated optogenetic constructs. In order to control for positive feedback, the native TGF-$\beta$ RII will be knocked out so that cells become insensitive to TGF-$\beta$ ligand. In so doing, we will be able to obtain complete control of TGF-$\beta$ signaling by light. 
\\
\newline
\uline{\textbf{Aim 2}} will capitalize on the ability to rapidly terminate the initiation of TGF-$\beta$ signaling in our optogenetic system to dissect the deactivation dynamics of TGF-$\beta$ signaling. Unlike ligand-based 
\\
\newline
\uline{\textbf{Aim 3}} will focus on the dynamics of p21, a key downstream effector of TGF-$\beta$ induced cytostasis. 


\subsection{SIGNIFICANCE}
\label{sec-2-2}
\textbf{Overview.} The tumor suppressive effect of TGF-$\beta$ signaling is a key step that needs to be overcome in cancers. Various mechanisms by which cells overcome TGF-$\beta$ induced cytostasis has been described. One mechanism by which cells are able to overcome the early-stage tumor suppressive effect of TGF-$\beta$ signaling yet retain responsiveness to TGF-$\beta$ in late-stage carcinogenesis found in pancreatic cancer is via the attenuation of TGF-$\beta$ signaling\cite{nicolas2003attenuation}. However, the precise mechanism by which the attenuation of TGF-$\beta$ signaling leads to the overcoming of TGF-$\beta$ induced cytostasis remains poorly understood.

\textbf{Near term outcomes:} Within the scope of this project, we will identify key steps that regulate the dynamics of TGF-$\beta$ induced cytostasis. Importantly, we will identify key feedback loops that dynamically regulate TGF-$\beta$ cytostasis. The identification of these steps allows us to develop novel therapeutics that will restore the tumor suppressive effect of TGF-$\beta$ signaling in cancer cells with attenuated TGF-$\beta$ signaling. Also, to the best of our knowledge, the model proposed in this project represents the first model to incorporate the dynamics of cell signaling with a signaling outcome. 

\textbf{Future outcomes:} Although our model developed in this project relates TGF-$\beta$ signaling with cell-cycle arrest in HaCaT, our model can potentially be extended to study other TGF-$\beta$ induced phenotypes such as ephithelial-mesenchymal transition and cell migration. Additionally, the framework provided here can potentially be extended to the study of other signaling pathways, as this project represents the first attempt to quantitatively model signaling dynamics with signaling outcome.

\subsection{APPROACH}
\label{sec-2-3}
The TGF-$\beta$ pathway has been extensively studied since it was first discovered. Although various isoforms of the TGF-$\beta$ ligand has been discovered and the molecular pathways delineated, most studies have focused on the TGF-$\beta$1 ligand as it is most implicated in diseases.

Both ligand-based and chemical biology based approaches have been developed to induce TGF-$\beta$ signaling. 

Optogenetics is a new tool that enables precise, rapid and reversible activation of cell signaling. First used by the Wendall laboratory to study the \emph{Ras} pathway\cite{toettcher2013using}, optogenetics has since been extended to numerous other pathways including the fibroblast growth factor (FGF) pathway\cite{grusch2014spatio}. 
\newline 
\textbf{Aim 1. Validation of optogenetic system and establishment of stable HaCaT cell line.}
\newline
\textbf{Molecular design.} In the chemical biology approach\cite{stockwell1998probing}, the cytoplasmic domain of the Type I receptor was to the FKBP12 protein and anchored to the plasma membrane while the cytoplasmic domain of TGF-b Type II receptor was fused to the protein FRS. Membrane associated hetero-dimerisation of FRS and FKPB12 was induced with the small molecule rapamycin, and sufficient to drive TGF-$\beta$ signaling. We adopted a similar approach to that by Stockwell and Schreiber, but instead replacing the chemical dimerisation of FKBP12 and FRS with light-induced dimerisation of PhyB-PIF. 

Our optogenetics constructs (Figure 2), TGF-$\beta$ RI(155-503)-10aaLinker-mCherry-10aaLinker-PhyB(1-908)-10aaLinker-Kras4BCT and TGF-$\beta$ RII(193-567)- 20aaLinker-mYFP-PIF6APB, were cloned into the pcDNA3.1A vector for transient transfection. When HaCaT cells were transiently transfected and exposed to light, we observed the induction of TGF-$\beta$ signaling that is able to recapitulate ligand-induced TGF-$\beta$ signaling (see \textbf{PRELIMINARY STUDIES}).

Because the PhyB-PIF system requires the presence of an exogenous phytochrome (PCB) with a short half-life of 1-2 hours, we also tested the ability of cells to synthesize PCB \emph{in-situ} as described in elsewhere\cite{muller2013synthesis}. Our results (see \textbf{PRELIMINARY STUDIES}) demonstrate that the \emph{in-situ} synthesis of PCB in HaCaT was able to render cells sensitive to light for up to 24 hours post-transfection, hence allowing us to interrogate TGF-$\beta$ induced cytostasis that has been shown to occur in the time-scale of 14-16 hours post-activation.
\\
\newline
\textbf{Stable cell line construction.} The constructs that have been validated in transient transfection experiments (Figure 1, and Preliminary Studies) will be cloned into the gateway-pENTR system and then recombined into the pLenti-puro destination vector. FACS will then be used to isolate cells that contain both mCherry and YFP, indicating that both constructs have been successfully transfected into the cell. CRISPR-KO will then be used to knockout TGF$\beta$ RII\cite{kato1995human}. 
\\
\newline
\textbf{Aim 2. Interrogating deactivation dynamics of TGF-$\beta$ signaling.} 
\newline

\\
\newline
\textbf{Aim 3. Quantification of p21 transcription and translation dynamics.} p21 mRNA will be quantified using the SmartFlare system\cite{seferos2007nano}. Unlike RT-qPCR, the SmartFlare system allows for the quantification of mRNA at the single-cell level. Likewise, p21 protein levels will be monitored using a p21-GFP fusion protein. For both mRNA and protein quantification, live-cell imaging on a confocal microscope will be used to monitor cellular fluorescence. Finally, using our data on p21 transcription and translation dynamics, we will develop a model of p21 dynamics, make testable predictions of how the dynamics of p21 is regulated. 

Because the dynamics of TGF-$\beta$ signaling is tightly coupled to the the dynamics of p21 induction, we will integrate our model of p21 regulation with existing models of TGF-$\beta$ signaling.Various models\cite{wang2014self,warmflash2012dynamics,zi2011quantitative} have been developed in HaCaT cell lines to model different aspects of TGF-$\beta$ signaling. We will also refine existing models of TGF-$\beta$ signaling by incorporating information pertaining to the deactivation of TGF-$\beta$ signaling. 

\subsection{INNOVATION}
\label{sec-2-4}
Optogenetics is increasingly used to interrogate signaling dynamics, due to the unprecedented levels of spatiotemporal control conferred. Although optogenetics has been extended to various pathways, this study is the first to achieve optogenetic control of the TGF-$\beta$ signaling pathway. Furthermore, the rapid onset of activation and deactivation (in the time-scale of seconds) makes our proposed system well-suited for studying TGF-$\beta$ deactivation dynamics. 

The other innovation proposed in this project is the development of a quantitative model that relates signaling dynamics with signaling outcome. Although mathematical models have been successfully used to make predictions of regulatory modules\cite{wang2014self,zi2011quantitative} within the TGF-$\beta$ signaling pathway, no model been extended to incorporate information the dynamics of downstream events. In extending the existing models of TGF-$\beta$ signaling to include the dynamics of p21 induction, our model is better suited to identify critical feedback loops that temporally regulate TGF-$\beta$ induced cell-cycle arrest. 

\subsection{INVESTIGATOR}
\label{sec-2-5}
\textbf{Principal Investigator.} Greg Tucker-Kellogg (GTK) brings with him 15 years of prior experience in biotechnology and pharmaceutical research. The GTK lab in the Department of Biological Sciences studies genetic switches that underlie differences in TGF-$\beta$ signaling outcome. To solve this problem, both experimental and computational approaches are used in synergy. The GTK lab has also developed and validated the optogenetics system that will be used in the work of this grant. 

\textbf{Co-investigator.} Assistant Professor Lisa Tucker-Kellogg (LTK) at Duke-NUS Graduate Medical School specializes in computational modeling of complex diseases. She has also published one of the most detailed model of TGF-$\beta$ signaling. Her expertise in mathematical modeling will play an important role in the development of our proposed model. 

\textbf{Collaborators.} Assistant Professor Pieter Eichhorn (PE) leads a laboratory devoted to the molecular biology of TGF-$\beta$ signaling at the Cancer Sciences Institute. His work focuses on the role of deubiquitinases as key regulators of TGF-$\beta$ signaling dynamics. He has developed knock-outs of various members of the SMAD pathway as well as experience in stable transfections. Within the scope of this grant, he will play a critical role in the establishment of the stable HaCaT cell line for our studies.

The investigators and collaborators involved in this project possesses the broad range of skills and expertise required for the successful execution of the work proposed in this grant. 

\subsection{ENVIRONMENT}
\label{sec-2-6}
The proposed research will be performed in the Greg Tucker-Kellogg (GTK) lab in the Department of Biological Sciences, with regular engagement of the Lisa Tucker-Kellogg at the Duke-NUS Graduate Medical School and the Eichhorn lab in the Cancer Sciences Institute. The labs are equipped for mammalian cell-based experiments, as well as standard molecular biology and bacterial genetics experiments. The GTK lab has experience with cellular imaging and bioinformatics, and will drive the collection of experimental data for model development. The Lisa Tucker-Kellogg lab has extensive experience in developing models of TGF-$\beta$ signaling, and will provide the expertise in developing our proposed model relating TGF-$\beta$ signaling with cell-cycle arrest. The Eichhorn lab has previous experience in creating stable cell lines using lentiviral infection as well as performing CRISPR knock-out, and will play a critical role in the construction of stable cell line. 

Cell imaging work will be performed using microscopy facilities of the NUS Centre for Bioimaging Sciences (CBIS), which is equipped with a range of microscopy systems catered for live-cell fluorescence imaging. Computational work will be done on the NUS server systems at the PI's laboratory house in CBIS. Open-source Linux-based  software packages will be used for our computational work, hence avoiding software licensing fees. 
\subsection{PRELIMINARY STUDIES}
\label{sec-2-7}
\bibliography{AcRF_T1}
% Emacs 24.3.1 (Org mode 8.2.10)
\end{document}